<?php

/**
 * Implementation of hook_drush_command().
 */
function taxonomy_export_drush_command() {
  $items = array();
  $items['taxonomy-export-import'] = array(
    'description' => 'Imports a Drupal taxonomy vocabulary exported by Taxonomy Export module.',
    'aliases' => array('te-import'),
    'callback' => 'drush_taxonomy_export_import',
    'arguments' => array(
      'export file' => 'Filename of the exported Drupal taxonomy.',
    ),
    'examples' => array(
      'drush taxonomy-export-import vocabulary.taxonomy_export.inc' => 'Imports the Drupal taxonomy vocabulary "Vocabulary Name".',
    ),
  );
  $items['taxonomy-export-export'] = array(
    'description' => 'Exports a Drupal taxonomy vocabulary using Taxonomy Export.',
    'aliases' => array('te-export'),
    'callback' => 'drush_taxonomy_export_export',
    'arguments' => array(
      'vid' => 'Vocabulary ID of a Drupal taxonomy.',
      'export-file' => 'Path to write the Taxonomy Export code to, this is required when not using --pipe mode.',
    ),
    'options' => array(
      'pipe' => 'Displays the raw export, useful for redirecting the export into a file.',
      'include-terms' => 'Should the export include terms also?',
    ),
    'examples' => array(
      'drush taxonomy-export-export 1' => 'Export the Drupal taxonomy vocabulary with Vocabulary ID = 1.',
      'drush taxonomy-export-export --pipe 1 > my-export.taxonomy_export.inc' => "Export the Drupal taxonomy vocabulary with Vocabulary ID = 1 and put it in the file 'my-export.taxonomy_export.inc'.",
    ),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function taxonomy_export_drush_help($section) {
  switch ($section) {
    case 'drush:taxonomy-export-import':
      return dt("Imports taxonomies exported by Taxonomy Export module. Usage: 'drush taxonomy-export-import export-filename'.");
  }
}

/**
 * Drush callback; import taxonomy vocabulary
 */
function drush_taxonomy_export_import() {
  $args = func_get_args();
  if (empty($args)) {
    return drush_set_error(dt('No taxonomy import file specified.'));
  }

  $file = $args[0];
  if (!file_exists($file)) {
    // Perhaps the file has a relative path.
    $file = realpath(drush_cwd() . DIRECTORY_SEPARATOR . $file);
    if (!file_exists($file)) {
      return drush_set_error(dt('File !file not found.', array('!file' => $args[0])));
    }
  }

  $form_state = array(
    'values' => array(
      'op' => t('Import'),
      'import_data' => file_get_contents($file),
    ),
  );

  module_load_include('inc', 'taxonomy_export', 'taxonomy_export.pages');

  if (_taxonomy_export_prepare_import_data($form_state['values']['import_data'])) {
    taxonomy_export_import_submit(array(), $form_state);
    $messages = drupal_get_messages();
    foreach ($messages as $type => $message_array) {
      foreach ($message_array as $message) {
        drush_log($message, 'ok');
      }
    }
  }
  else {
    return drush_set_error(dt('Could not parse import data in "!file".', array('!file' => $file)));
  }
}

/**
 * Drush callback; import taxonomy vocabulary
 */
function drush_taxonomy_export_export() {
  $args = func_get_args();
  if (empty($args)) {
    return drush_set_error(dt('No Vocabulary ID specified.'));
  }
  else if (!drush_get_context('DRUSH_PIPE') && !$args[1]) {
    return drush_set_error(dt('No export-file path specified, use --pipe mode if you want to use the export as a stream.'));
  }

  $form_state = array(
    'values' => array(
      'vid' => $args[0],
      'include_terms' => (bool) drush_get_option('include-terms'),
      'as_file' => 0,
    )
  );
  $vocabulary = taxonomy_vocabulary_load($args[0]);

  module_load_include('inc', 'taxonomy_export', 'taxonomy_export.pages');
  taxonomy_export_export_submit(array(), $form_state);

  if (drush_get_context('DRUSH_PIPE')) {
    drush_print_pipe($form_state['storage']['taxonomy_export']);
  }
  else {
    // Absolute paths should be left alone, prepend CWD to all others
    $path = (substr($args[1], 0, 1) !== '/') ? drush_cwd() . '/' . $args[1] : $args[1];

    if (file_put_contents($path, $form_state['storage']['taxonomy_export'])) {
      drush_log(dt($path . ' Sucessfully exported vocabulary: @name', array('@name' => $vocabulary->name)), 'ok');
    }
    else {
      return drush_set_error(dt('Was not able to write the taxonomy export to: @path', array('@path' => $args[1])));
    }
  }
}

